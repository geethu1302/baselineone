package bl.framework.testcases;

import org.testng.annotations.Test;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class TC006_Facebook {

	@Test
	public void searchTestleaf() {
	
		System.setProperty("webdriver.chrome.driver","./Drivers\\chromedriver.exe");
		ChromeOptions op=new ChromeOptions();
		op.addArguments("--disable-notifications");
		ChromeDriver driver=new ChromeDriver(op);
		
		driver.manage().window().maximize();
		driver.get("https://www.facebook.com");
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		driver.findElementById("email").sendKeys("higeethumails@gmail.com");
		driver.findElementById("pass").sendKeys("iftomorrowcomes");
		driver.findElementById("loginbutton").click();
		WebDriverWait wait=new WebDriverWait(driver,20);
		driver.findElementByXPath("//input[@data-testid='search_input']").sendKeys("Testleaf");
		driver.findElementByXPath("//button[@data-testid='facebar_search_button']").click();
		
		WebElement places = driver.findElementByXPath("//div[text()='Places']/parent::div/parent::a");
		//WebDriverWait wait=new WebDriverWait(driver,30);
	      //wait.until(ExpectedConditions.elementToBeClickable(places));
		places.click();
		
		System.out.println(driver.findElementByXPath("//a[contains(text(),'TestLeaf')]").getText());
		
		String text = driver.findElementByXPath("//span[@data-bt='{\"ct\":\"place_name\"}']/a").getText();
		if(text.equalsIgnoreCase("Testleaf"))
			System.out.println("Text verified");
		
				
		String likeButtonText = driver.findElementByXPath("(//button[contains(@class,'like')])[1]").getText();
		System.out.println(likeButtonText);
		if(likeButtonText.equals("Like"))
			 driver.findElementByXPath("(//button[contains(@class,'like')])[1]").click();
		else
			System.out.println("Already Liked");
		
		driver.findElementByXPath("//a[contains(text(),'TestLeaf')]").click();
		
		String title = driver.getTitle();
		if(title.contains("Testleaf"))
			System.out.println("Title  verified");
		String likeText = driver.findElementByXPath("//div[contains(text(),'people like this')]").getText();
		
		String likeText2 = likeText.replaceAll("[a-zA-Z]", "");
		System.out.println("There are"+likeText2+"likes for this page");
//		driver.close();
	}
}

package bl.framework.testcases;

import bl.framework.api.SeleniumBase;
import bl.framework.design.ProjectMethods;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


public class TC004_DuplicateLead extends ProjectMethods {
	@BeforeTest(groups="common")
	
	public void setData() {
		
		 testcaseName="TC004_DuplicateLead";
		 desc="To delete a lead in LeafTaps";
		 author="Keerthana";
		 cat="Smoke";
		
	}
@Test(groups="reg")
public void duplicateLead() {
	
	/*startApp("chrome", "http://leaftaps.com/opentaps");
	WebElement eleUsername = locateElement("id", "username");
	clearAndType(eleUsername, "DemoSalesManager"); 
	WebElement elePassword = locateElement("id", "password");
	clearAndType(elePassword, "crmsfa"); 
	WebElement eleLogin = locateElement("class", "decorativeSubmit");
	click(eleLogin); 
	
	
	WebElement eleCrmsfa = locateElement("partiallinktext","CRM/SFA");
	click(eleCrmsfa);*/
	
	//login();
	WebElement eleCreateLead = locateElement("partiallinktext","Create Lead");
	click(eleCreateLead);
	
	WebElement eleFindLeads = locateElement("xpath","//a[text()='Find Leads']");
	clickWithoutSnap(eleFindLeads);
	
	WebElement eMailTab = locateElement("xpath", "//span[text()='Email']");
	clickWithoutSnap(eMailTab);
	
	WebElement eMailText = locateElement("xpath", "//input[@name='emailAddress']");
	clearAndType(eMailText, "higeethumails@gmail.com");
	
	
	WebElement findLeadsButton = locateElement("xpath", "//button[text()='Find Leads']");
	clickWithoutSnap(findLeadsButton);
	
	WebElement leadID = locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-firstName ']/div/a");
	
			
	clickWithoutSnap(leadID);
	
	
	
				
	WebElement duplicateLeadButton = locateElement("xpath", "//a[text()='Duplicate Lead']");
	clickWithoutSnap(duplicateLeadButton);
	
	if(driver.getTitle().contains("Duplicate Lead"))
		System.out.println("Page Title Verified");
	
	WebElement createLeadDup = locateElement("xpath", "//input[@name='submitButton']");
	click(createLeadDup);
	
	
	
	WebElement idFirstName = locateElement("xpath", "//span[@id='viewLead_firstName_sp']");
	
	     
	if(idFirstName.getText().equals("Keerthana"))
			System.out.println("Verified the Duplicated Lead Name is same");
	
	driver.close();
}
	
}

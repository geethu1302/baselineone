package bl.framework.testcases;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
//import org.openqa.selenium.interactions.ClickAndHoldAction;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;
import bl.framework.design.ProjectMethods;

public class TC001_LoginAndLogout extends ProjectMethods{

	@BeforeTest(groups="common")
	
	public void setData() {
		
		 testcaseName="TC001_CreateLead";
		 desc="To create a lead in LeafTaps";
		 author="Keerthana";
		 cat="Smoke";
		 workbookName="Testdata";
		 datasheetName="CreateLead";
	
	}

	//@Test(invocationCount=2,invocationTimeOut=30000)
	@Test(groups="smoke",dataProvider="fetchData")
	
	public void createLead(String cname,String fname,String lname) {
		//login();
		WebElement eleCreateLead = locateElement("partiallinktext","Create Lead");
		click(eleCreateLead);
		WebElement eleCompName = locateElement("id","createLeadForm_companyName");
		clearAndType(eleCompName, cname);
		WebElement eleFname=locateElement("id","createLeadForm_firstName");
		clearAndType(eleFname, fname);
		WebElement eleLname = locateElement("id", "createLeadForm_lastName");
		clearAndType(eleLname, lname);
		
		
		//WebElement eleCountry = locateElement("id","createLeadForm_generalCountryGeoId");
		//selectDropDownUsingText(eleCountry, "India");
		
		
		WebElement eleSubmitLead = locateElement("class","smallSubmit");
		click(eleSubmitLead);
		WebElement eleText = locateElement("xpath","//span[@id='viewLead_firstName_sp']");
		if(eleText.getText().contains("Keerthana"))
			System.out.println("Lead created successfully");
		
		else
			System.out.println("failed to create lead");
				
	}
	
	
	/*@DataProvider(name="getData")
	public String[][] fetchData(){
		
		String[][] data=new String[2][3];
		data[0][0]="Testleaf";
		data[0][1]="Gayatri";
		data[0][2]="c";
		
		data[1][0]="Testleaf";
		data[1][1]="Sarath";
		data[1][2]="c";
		
		
		return data;
	}*/
		
}









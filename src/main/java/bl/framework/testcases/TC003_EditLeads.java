package bl.framework.testcases;

import bl.framework.api.SeleniumBase;
import bl.framework.design.ProjectMethods;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TC003_EditLeads extends ProjectMethods {

@BeforeTest(groups="common")
	
	public void setData() {
		
		 testcaseName="TC003_EditLead";
		 desc="To edit a lead in LeafTaps";
		 author="Keerthana";
		 cat="Smoke";
	
	}
	
	//@Test(dependsOnMethods= {"bl.framework.testcases.TC001_LoginAndLogout.createLead"})
@Test(groups="smoke")
public void editLead() {
		/*startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, "DemoSalesManager"); 
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa"); 
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin); 
		
		
		WebElement eleCrmsfa = locateElement("partiallinktext","CRM/SFA");
		click(eleCrmsfa);*/
		//login();
		WebElement eleCreateLead = locateElement("partiallinktext","Create Lead");
		click(eleCreateLead);
		
		WebElement eleFindLeads = locateElement("xpath","//a[text()='Find Leads']");
		clickWithoutSnap(eleFindLeads);
			      
		//Locate first name
				
		
		WebElement eleFname = locateElement("xpath","(//input[@name='firstName'])[3]");
				
		//WebDriverWait wait=new WebDriverWait(driver,30);
	    //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//input[@name='firstName'])[1]")));
	    
	    //click(eleFname);
		clearAndType(eleFname, "Keerthana");
		
		WebElement buttonFindLeads = locateElement("xpath","//button[text()='Find Leads']");
		click(buttonFindLeads);
		
		
		WebElement firstLeadID = locateElement("xpath", "(//a[@class='linktext'])[4]");
		
		WebDriverWait wait=new WebDriverWait(driver,30);
	      wait.until(ExpectedConditions.elementToBeClickable(firstLeadID));
		clickWithoutSnap(firstLeadID);
		
		if(driver.getTitle().contains("View Lead"))
			System.out.println("Verified Page Title");
		
		WebElement editButton = locateElement("xpath", "(//a[@class='subMenuButton'])[3]");
		click(editButton);
		
		WebElement companyName = locateElement("id", "updateLeadForm_companyName");
		clearAndType(companyName, "Disney");
		
		WebElement updateButton = locateElement("xpath", "(//input[@name='submitButton'])[1]");
		clickWithoutSnap(updateButton);
		
		WebElement compName = locateElement("xpath", "//span[@id='viewLead_companyName_sp']");
		if(compName.getText().contains("Disney"))
		{
			System.out.println("Company Name is updated");
		}
		
		driver.close();
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
}
}

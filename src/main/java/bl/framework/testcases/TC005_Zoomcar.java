package bl.framework.testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class TC005_Zoomcar {

	@Test
	public void bookJourney()
	{
		System.setProperty("webdriver.chrome.driver","./Drivers\\chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.get("https://www.zoomcar.com/chennai/");
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		driver.findElementByXPath("//a[@title='Start your wonderful journey']").click();
		driver.findElementByXPath("//div[text()='Popular Pick-up points']/following-sibling::div[1]").click();
		
		driver.findElementByXPath("//button[text()='Next']").click();
		
		Date d=new Date();
		DateFormat sdf=new SimpleDateFormat("dd");
		String today=sdf.format(d);
		int tomorrow=Integer.parseInt(today)+1;
		System.out.println(tomorrow);
		/*for(int i=1;i<4;i++)
		{
			//System.out.println(driver.findElementByXPath("(//div[@class='day low-price'])["+i+"]").getText());
		if((driver.findElementByXPath("(//div[@class='day low-price'])["+i+"]").getText()).contains(String.valueOf(tomorrow)));
		
			{
			driver.findElementByXPath("(//div[@class='day low-price'])["+i+"]").click();
			driver.findElementByXPath("//button[text()='Next']").click();
			break;
		}*/
		driver.findElementByXPath("//div[contains(text(),'"+tomorrow+"')]").click();
		driver.findElementByXPath("//button[text()='Next']").click();
		driver.findElementByXPath("//button[text()='Done']").click();
		
		List<WebElement> priceList = driver.findElementsByXPath("//div[@class='price']");
        List<String> priceList1=new ArrayList<>();
        for(int i=0;i<=priceList.size()-1;i++) {
        	 priceList1.add(priceList.get(i).getText().replaceAll("\\D", ""));
         
        }
        System.out.println(priceList1);
        String max = Collections.max(priceList1);
        System.out.println(max);
		
        
        System.out.println(driver.findElementByXPath("//div[contains(text(),'"+max+"')]/parent::div/parent::div/preceding-sibling::div[1]/h3").getText());
        ////button[text()='BOOK NOW']
        
        int index=priceList1.indexOf(max);
        System.out.println(index+1);
        int carIndex=index+1;
        driver.findElementByXPath("(//button[text()='BOOK NOW'])["+carIndex+"]").click();
        
       // driver.findElementByXPath("//div[contains(text(),'525')]/following-sibling::div/following-sibling::button").click();
        driver.close();
		
		}
	
	
	
}


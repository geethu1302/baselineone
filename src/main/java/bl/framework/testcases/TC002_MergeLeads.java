package bl.framework.testcases;

import bl.framework.api.SeleniumBase;
import bl.framework.design.ProjectMethods;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TC002_MergeLeads extends ProjectMethods {
@BeforeTest(groups="common")
	
	public void setData() {
		
		 testcaseName="TC002_MergeLead";
		 desc="To merge two leads in LeafTaps";
		 author="Keerthana";
		 cat="Smoke";
		
	}
	
	@Test
	//(groups="sanity")
public void MergeLeads() throws InterruptedException {
		
		/*startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, "DemoSalesManager"); 
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa"); 
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin); 
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		//click(eleLogout);
		WebElement eleCrmsfa = locateElement("partiallinktext","CRM/SFA");
		click(eleCrmsfa);*/
		//login();
		WebElement eleCreateLead = locateElement("partiallinktext","Create Lead");
		click(eleCreateLead);
		//merge leads
		WebElement eleMerge = locateElement("partiallinktext", "Merge Leads");
		click(eleMerge);
		
		
		
		
		
		WebElement eleFromLead = locateElement("xpath","//img[@src='/images/fieldlookup.gif']");
		click(eleFromLead);
		
		switchToWindow(1);
		
		WebElement eleLeadID = locateElement("xpath", "//input[@name='id']");
		clearAndType(eleLeadID, "10039");
		
		WebElement eleFindLeads = locateElement("xpath","(//button[@class='x-btn-text'])[1]");
		click(eleFindLeads);
		
		 
				
		List<WebElement> trows = locateElements("xpath","(//table[@class='x-grid3-row-table'])[1]"); 
	    WebElement firstRow = trows.get(0);
	    
       List<WebElement> tcols=  firstRow.findElements(By.tagName("td"));
       
      
       
       WebElement leadID = locateElement("xpath","(//a[@class='linktext'])[1]");
       
      WebDriverWait wait=new WebDriverWait(driver,30);
      wait.until(ExpectedConditions.elementToBeClickable(leadID));
      clickWithoutSnap(leadID);
      
       
       switchToWindow(0);
       
       
      
           
       //To Lead
       
       WebElement eleToLead = locateElement("xpath","(//img[@src='/images/fieldlookup.gif'])[2]");
       click(eleToLead);
       
       switchToWindow(1);
       
       WebElement eleLeadID1 = locateElement("xpath", "//input[@name='id']");
		clearAndType(eleLeadID1, "10040");
		
		WebElement eleFindLeads1 = locateElement("xpath","(//button[@class='x-btn-text'])[1]");
		click(eleFindLeads1);
		WebElement leadId1 = locateElement("xpath","(//a[@class='linktext'])[1]");
		WebDriverWait wait1=new WebDriverWait(driver,30);
	      wait.until(ExpectedConditions.elementToBeClickable(leadId1));
	      clickWithoutSnap(leadId1);
	      
	       
	       switchToWindow(0);
	      
	       WebElement mergeLeadButton = locateElement("xpath","//a[@class='buttonDangerous']");
	       clickWithoutSnap(mergeLeadButton);
	       
	       
	       acceptAlert();
       

		
		WebElement findLeads = locateElement("xpath", "//a[text()='Find Leads']");
		
		clickWithoutSnap(findLeads);
		WebElement idTxtBox = locateElement("xpath", "//input[@name='id']");
		clickWithoutSnap(idTxtBox);
		clearAndType(idTxtBox, "10039");
		
		WebElement findLeadsButton = locateElement("xpath", "//button[text()='Find Leads']");
		clickWithoutSnap(findLeadsButton);
		WebElement errorMsg = locateElement("xpath", "//div[@class='x-paging-info']");
		if(errorMsg.getText().contains("No records"))
		{
			System.out.println("Error message received");
		}
		
		
		
}


}

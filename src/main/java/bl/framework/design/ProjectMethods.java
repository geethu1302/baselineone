package bl.framework.design;

import org.openqa.selenium.WebElement;
import org.testng.annotations.*;

import bl.framework.api.SeleniumBase;
import utils.ReadExcel;

public class ProjectMethods extends SeleniumBase {

	public String testcaseName;
	public String desc;
	public String author;
	public String cat;
	public String datasheetName;
	public String workbookName;
	
	@Parameters({"url","username","password"})
	@BeforeMethod
	//(groups="common")
	public void login(String url,String uname,String pwd ) {
		startApp("chrome", url);
		WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, uname); 
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, pwd); 
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin); 
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		//click(eleLogout);
		WebElement eleCrmsfa = locateElement("partiallinktext","CRM/SFA");
		click(eleCrmsfa);
		
	}
	
	/*@AfterMethod(groups="common")
	public void closeApp() {
		close();

	}*/
	
	@BeforeSuite
	//(groups="common")
	public void beforeSuite() {
		startReport();
	}
	
	@AfterSuite
	//(groups="common")
	public void endSuite()
	{
		endReport();
		
	}
	
	@BeforeClass
	//(groups="common")
	public void beforeClass()
	{
		
		initializeTest(testcaseName, desc, author, cat);
		
	}
	
	@DataProvider(name="fetchData")
	public Object[][] getData(){
		return ReadExcel.readData(workbookName,datasheetName);
	}
}

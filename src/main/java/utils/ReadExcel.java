package utils;


import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	static Object[][] data;
	//public static void main(String[] args) throws IOException {
	public static Object[][] readData(String workbookName,String datasheetName){
		
		
		// TODO Auto-generated method stub

	
		XSSFWorkbook wbook;
		try {
			wbook = new XSSFWorkbook("./Data/"+workbookName+".xlsx");
		
		XSSFSheet sheet=wbook.getSheet(datasheetName);
		int rowCount=sheet.getLastRowNum();
		System.out.println("RowCount"+rowCount);
		int colCount=sheet.getRow(0).getLastCellNum();
		System.out.println("ColumnCount"+colCount);
		data=new String[rowCount][colCount];
		
		for(int i=1;i<=rowCount;i++)
		{
			XSSFRow row=sheet.getRow(i);
			for(int j=0;j<colCount;j++)
			{
				XSSFCell cell=row.getCell(j);
				data[i-1][j]=cell.getStringCellValue();
				
			}
		}
		wbook.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	return data;
	
	}
		
	}



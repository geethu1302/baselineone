package utils;


	import java.io.IOException;

	import org.testng.annotations.Test;

	import com.aventstack.extentreports.ExtentReports;
	import com.aventstack.extentreports.ExtentTest;
	import com.aventstack.extentreports.MediaEntityBuilder;
	import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
	public class AdvancedReport {

		static ExtentHtmlReporter html;
		static ExtentReports extent;
		ExtentTest test;
		
		
		public void startReport() {
			html=new ExtentHtmlReporter("./report/extentReport1.html");
			extent=new ExtentReports();
			html.setAppendExisting(true);
			extent.attachReporter(html);
		}
		public void initializeTest(String testcaseName,String desc,String author,String cat) {

			test=extent.createTest(testcaseName,desc);
			test.assignAuthor(author);
			test.assignCategory(cat);
			
		}
		public void logStep(String des,String status) {
			
			if(status.equalsIgnoreCase("pass")) {
				test.pass(des);
				
			}
			else if(status.equalsIgnoreCase("fail"))
			{
				test.fail(des);
			}
			else if(status.equalsIgnoreCase("warning"))
			{
				test.warning(des);
			}
			
		}
		public void endReport() {
				extent.flush();	
			}
			
			
				
			
		}
		
	

